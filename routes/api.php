<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/v1', 'namespace' => 'api\v1', 'as' => 'api.'], function () {
    Route::resource('doctors', 'DoctorsController', ['except' => ['create', 'edit']]);
    Route::post('getDoctors', 'DoctorsController@getDoctors');

    Route::resource('clients', 'ClientsController', ['except' => ['create', 'edit']]);
    Route::post('getClients', 'ClientsController@getClients');

    Route::resource('settings', 'SettingController', ['except' => ['create', 'edit']]);
    Route::post('getSetting', 'SettingController@getSetting');

    Route::resource('templates', 'TemplatesController', ['except' => ['create', 'edit']]);
    Route::post('getTemplates', 'TemplatesController@getTemplates');

    Route::resource('clients/card', 'CardsController', ['except' => ['create', 'edit']]);
    Route::post('getDiagnosis', 'CardsController@getDiagnosis');

    Route::resource('clients/xray', 'XraysController', ['except' => ['create', 'edit']]);
    Route::post('addXray', 'XraysController@addXray');
});

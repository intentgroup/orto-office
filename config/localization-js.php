<?php

return [

    /*
     * Set the names of files you want to add to generated javascript.
     * Otherwise all the files will be included.
     *
     * 'messages' => [
     *     'validation',
     *     'forum/thread',
     * ],
     */
    'messages' => [
        'component/doctor',
        'component/client',
        'component/template',
    ],

    /*
     * The default path to use for the generated javascript.
     */
    'path' => resource_path('assets/js/vue-translations.js'),
];

<?php

return [

    'name' => 'Name',
    'phone' => 'Phone',
    'address' => 'Address',
    'email' => 'Email',
    'birthday' => 'Birth day',
    'additional' => 'Additional',

];
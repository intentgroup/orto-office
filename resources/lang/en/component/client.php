<?php

return [

    'name' => 'Name',
    'last_name' => 'Last Name',
    'patronymic' => 'Patronymic',
    'card_number' => 'Card Number',
    'male' => 'Male',
    'phone' => 'Phone',
    'country' => 'Country',
    'city' => 'City',
    'email' => 'Email',
    'address' => 'Address',
    'birthday' => 'Birthday',

];

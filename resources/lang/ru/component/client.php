<?php

return [

    'name' => 'Имя',
    'last_name' => 'Фамилия',
    'patronymic' => 'Отчество',
    'card_number' => 'Номер карточки',
    'male' => 'Пол',
    'phone' => 'Телефон',
    'country' => 'Страна',
    'city' => 'Город',
    'address' => 'Адресс',
    'email' => 'Email',
    'birthday' => 'Дата рождения',

];

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

var Lang = require('lang.js');
window.Langgg = require('lang.js');
import translations from './../assets/js/vue-translations.js';
var lang = new Lang();

lang.setLocale(locale);
lang.setMessages(translations);


window.Vue = require('vue');

Vue.filter('trans', (...args) => {

    return lang.get(...args);
});
import VueRouter from 'vue-router';
import VCalendar from 'v-calendar';
window.Vue.use(VueRouter);
window.Vue.use(VCalendar);

import DoctorsIndex from './components/doctors/doctorsIndex.vue';
import DoctorsCreate from './components/doctors/doctorsCreate.vue';
import DoctorsEdit from './components/doctors/doctorsEdit.vue';
import DoctorsRegistry from './components/doctors/doctorsRegistry.vue';

import ClientsIndex from './components/clients/clientsIndex.vue';
import ClientsCreate from './components/clients/clientsCreate.vue';
import ClientsEdit from './components/clients/clientsEdit.vue';
import ClientsCard from './components/clients/clientsCard.vue';

import ClinicIndex from './components/settings/settingsIndex.vue';
import TemplateIndex from './components/settings/templatesIndex.vue';
import TemplateCreate from './components/settings/templatesCreate.vue';
import TemplateEdit from './components/settings/templatesEdit.vue';

import DiagnosisEdit from './components/diagnosis/diagnosisEdit.vue';
import DiagnosisCreate from './components/diagnosis/diagnosisCreate.vue';



const routes = [
    {
        path: '/',
        components: {
            DoctorsIndex: DoctorsIndex
        }
    },
    {path: '/admin/doctors/create', component: DoctorsCreate, name: 'createDoctor'},
    {path: '/admin/doctors/edit/:id', component: DoctorsEdit, name: 'editDoctor'},
    {path: '/admin/doctors/registry/:id', component: DoctorsRegistry, name: 'registryDoctor'},

    {path: '/clients', component: ClientsIndex, name: 'ClientsIndex'},
    {path: '/admin/clients/create', component: ClientsCreate, name: 'createClient'},
    {path: '/admin/clients/edit/:id', component: ClientsEdit, name: 'editClient'},
    {path: '/admin/clients/card/:id', component: ClientsCard, name: 'editCard'},
    {path: '/admin/clients/card/:id/create/', component: DiagnosisCreate, name: 'createDiagnosis' },
    {path: '/admin/clients/card/:id/edit/:nid', component: DiagnosisEdit, name: 'editDiagnosis' },

    {path: '/settings', component: ClinicIndex, name: 'clinicIndex'},
    {path: '/settings/templates', component: TemplateIndex, name: 'templatesIndex'},
    {path: '/settings/templates/create', component: TemplateCreate, name: 'createTemplate'},
    {path: '/settings/templates/edit:id', component: TemplateEdit, name: 'editTemplate'},
]

const router = new VueRouter({ routes })

const app = new Vue({ router }).$mount('#app')

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="form-group">
                        <router-link :to="{name: 'clinicIndex'}" class="btn btn-info">Settings</router-link>
                    </div>
                    <h1 class="panel-heading">Admin Panel</h1>
                    <input type="hidden" id="user_id" name="user_id" value={{ Auth::user()->id }}>
                    <div class="panel-body">
                        <router-view name="DoctorsIndex"></router-view>
                        <router-view></router-view>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    let locale = '{{ config('app.locale') }}';
</script>

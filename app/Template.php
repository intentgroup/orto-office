<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{

    protected $fillable = [
        'name', 'user_id', 'complaints',
        'description', 'treatment'
    ];
}

<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = [
        'name', 'user_id', 'phone', 'address', 'email', 'birthday', 'additional'
    ];

    public function users(){
        return $this->belongsTo(User::class);
    }
}

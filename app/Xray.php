<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xray extends Model
{
    protected $fillable = [
        'client_id', 'image', 'date'
    ];
}

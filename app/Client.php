<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'last_name',
        'patronymic',
        'card_number',
        'male',
        'phone',
        'country',
        'city',
        'address',
        'email',
        'birthday',
    ];

    public function users(){
        return $this->belongsTo(User::class);
    }
}

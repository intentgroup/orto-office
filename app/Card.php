<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
        'name', 'user_id', 'complaints', 'type','date',
        'description', 'treatment'
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doctor;

class DoctorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return Doctor::all();
    }

    public function getDoctors(Request $request)
    {   
        
        $doctors = new Doctor;
        return $doctors->where('user_id', $request->user_id)->get();;
    }
 
    public function show($id)
    {
        return Doctor::findOrFail($id);
    }
 
    public function update(Request $request, $id)
    {
        $doctor = Doctor::findOrFail($id);
        $doctor->update($request->all());
 
        return $doctor;
    }
 
    public function store(Request $request)
    {
        $doctor = Doctor::create($request->all());
        return $doctor;
    }
 
    public function destroy($id)
    {
        $doctor = Doctor::findOrFail($id);
        $doctor->delete();
        return '';
    }
}

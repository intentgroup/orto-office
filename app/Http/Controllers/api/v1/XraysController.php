<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Xray;

class XraysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $xray = Xray::findOrFail($id);
        $xray->delete();
        return '';
    }

    public function addXray(Request $request)
    {
        if ($request->hasfile('files')) {
            foreach ($request->file('files') as $image) {

                $path = $request->client_id;
                $name = $image->getClientOriginalName();
                $image->move(public_path() . '/images/client' . $path . '/', $name);
                $image_url = '/images/client' . $path . '/' . $name;
                $xray = Xray::create([
                    'client_id' => $path,
                    'image' => $image_url,
                    'date' => $request->date,
                ]);
            }
        }
        return $xray->where('client_id', $request->client_id)->get();
    }
}

<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Diagnosis;
use App\Xray;


class CardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return Diagnosis::all();
    }

    public function getDiagnosis(Request $request)
    {
        $diagnosis = new Diagnosis;
        $data['diagnosis'] = $diagnosis->where('client_id', $request->client_id)->get();
        $xrays = new Xray;
        $data['xrays'] = $xrays->where('client_id', $request->client_id)->get();

        return $data;
    }

    public function show($id)
    {
        $diagnos = Diagnosis::findOrFail($id);
        $diagnos->files = json_decode($diagnos->files);
        return $diagnos;
    }

    public function update(Request $request, $id)
    {
        $diagnosis = Diagnosis::findOrFail($id);
        $diagnosis->update($request->all());
        return $diagnosis;
    }

    public function store(Request $request)
    {
        $diagnosis = Diagnosis::create($request->all());
        return $diagnosis;
    }

    public function destroy($id)
    {
        $diagnosis = Diagnosis::findOrFail($id);
        $diagnosis->delete();
        return '';
    }

}

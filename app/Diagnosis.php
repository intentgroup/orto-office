<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    protected $fillable = [
        'name', 'user_id', 'client_id', 'complaints', 'type', 'date', 'file',
        'description', 'treatment',
    ];

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
